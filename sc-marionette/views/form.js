var FormView = Mn.View.extend({
	tagName: 'form',
	template: _.template('\
		<label for="id_tarefa">Tarefa:</label>\
		<input type="text" name="tarefa" id="id_tarefa" />\
		<label for="id_resp">Responsável:</label>\
		<input type="text" name="resp" id="id_resp" />\
		<button id="btn-add">Adicionar</button>\
		'),

	ui: {
		tarefa: '#id_tarefa',
		responsavel: '#id_resp',
	},

	triggers: {
		'submit': 'add:tarefa'
	},

	modelEvents:{
		change: 'render'
	},
});