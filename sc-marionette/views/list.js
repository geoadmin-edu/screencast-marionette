var ToDo = Mn.View.extend({
	tagName: 'li',
	template: _.template("<%- tarefa %> - <%- responsavel %>")
});


var TodoList = Mn.CollectionView.extend({
	tagName: 'ul',
	childView: ToDo
});