var TodoModel = Backbone.Model.extend({
	defaults: {
		tarefa: '',
		responsavel: ''
	},

	validate: function(attrs){
		var errors = {};
		var hasError = false;

		if(!attrs.tarefa){
			errors.tarefa = 'tarefa não pode ser nula';
			hasError = true;
		}

		if(!attrs.responsavel){
			errors.responsavel = 'responsável não pode ser nulo';
			hasError = true;
		}

		if(hasError){
			return errors;
		}
	}
});