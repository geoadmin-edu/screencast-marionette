var initialData = [
		{tarefa: 'dormir', responsavel: 'josé'},
		{tarefa: 'acordar', responsavel: 'josé'}
	];

var app = new Mn.Application({
	onStart: function(options){
		var todo = new Layout({
			collection: new Backbone.Collection(options.initialData),
			model: new TodoModel()
		});

		todo.render();
		todo.triggerMethod('show');
	}
});

app.start({initialData: initialData});
