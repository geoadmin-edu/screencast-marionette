var Layout = Mn.View.extend({
	el: '#app',
	template: _.template('\
		<div class="list"></div>\
		<div class="form"></div>\
	'),
	
	regions: {
		form: '.form',
		list: '.list'
	},

	collectionEvents:{
		add: 'tarefaAdicionada'
	},

	onShow: function(){
		var formView = new FormView({model: this.model});
		var listView = new TodoList({collection: this.collection});

		this.showChildView('form', formView);
		this.showChildView('list', listView);
	},

	onChildviewAddTarefa: function(child){
		this.model.set({
			tarefa: child.ui.tarefa.val(),
			responsavel: child.ui.responsavel.val()
		}, {validate:true});

		var items = this.model.pick('tarefa', 'responsavel');
		this.collection.add(items);
	},

	tarefaAdicionada: function(){
		this.model.set({
			tarefa: '',
			responsavel: ''
		});
	}
});