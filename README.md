<h5>Marionette JS</h5>
-
Este projeto serve de base para o screencast sobre Marionette, com vídeo disponível neste [link](https://www.youtube.com/watch?v=-8TAMl4yFgs&list=PLFEjjtdJaE1rbVI7lY9Wld6uCKIJhNP_v&index=8), 
com a apresentação neste [link](http://slides.com/phillipepereira/marionette#/).

A apresentação destaca as bibliotecas que precisam ser instaladas e alguns conceitos úteis para entendimento do código. Apresenta também as principais referências consultadas.
